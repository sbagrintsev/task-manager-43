package ru.tsc.bagrintsev.tm.dto.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "m_project")
@NoArgsConstructor
public final class ProjectDTO extends AbstractWBSModelDTO {

}
