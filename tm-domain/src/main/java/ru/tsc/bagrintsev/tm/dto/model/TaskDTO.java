package ru.tsc.bagrintsev.tm.dto.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@Entity
@Table(name = "m_task")
@NoArgsConstructor
public final class TaskDTO extends AbstractWBSModelDTO {

    @Nullable
    @Column(name = "project_id")
    private String projectId;

}
