package ru.tsc.bagrintsev.tm.model;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
@Entity
@Table(name = "m_task")
@NoArgsConstructor
public final class Task extends AbstractWBSModel {

    @Nullable
    @ManyToOne
    private Project project;

}
