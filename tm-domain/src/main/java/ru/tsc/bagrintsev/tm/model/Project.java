package ru.tsc.bagrintsev.tm.model;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "m_project")
@NoArgsConstructor
public final class Project extends AbstractWBSModel {

    @NotNull
    @OneToMany(mappedBy = "project")
    private List<Task> tasks = new ArrayList<>();

}
