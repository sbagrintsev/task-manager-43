package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.system.AboutRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.InfoRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.VersionRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerInfoResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerVersionResponse;

import static ru.tsc.bagrintsev.tm.util.FormatUtil.formatBytes;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    public SystemEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final AboutRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerInfoResponse getInfo(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final InfoRequest request
    ) {
        @NotNull final Runtime runtime = Runtime.getRuntime();
        @NotNull final String processors = String.format("Available processors (cores): %d", runtime.availableProcessors());
        final long freeMemoryLong = runtime.freeMemory();
        @NotNull final String freeMemory = String.format("Free memory: %s", formatBytes(freeMemoryLong));
        final long maxMemoryLong = runtime.maxMemory();
        final boolean isMaximum = maxMemoryLong == Long.MAX_VALUE;
        @NotNull final String maxMemoryStr = isMaximum ? "no limit" : formatBytes(maxMemoryLong);
        @NotNull final String maxMemory = String.format("Maximum memory: %s", maxMemoryStr);
        final long totalMemoryLong = runtime.totalMemory();
        @NotNull final String totalMemory = String.format("Total memory available to JVM: %s", formatBytes(totalMemoryLong));
        final long usedMemoryLong = totalMemoryLong - freeMemoryLong;
        @NotNull final String usedMemory = String.format("Used memory in JVM: %s", formatBytes(usedMemoryLong));
        @NotNull final ServerInfoResponse response = new ServerInfoResponse();
        response.setProcessors(processors);
        response.setFreeMemory(freeMemory);
        response.setMaxMemory(maxMemory);
        response.setTotalMemory(totalMemory);
        response.setUsedMemory(usedMemory);
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final VersionRequest request
    ) {
        @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

}

