package ru.tsc.bagrintsev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @Override
    void add(@NotNull final Project record);

    @Override
    void addAll(@NotNull final Collection<Project> records);

    @Override
    void clear(@NotNull final String userId);

    @Override
    void clearAll();

    @Override
    boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    @Nullable
    List<Project> findAll();

    @Override
    @Nullable
    List<Project> findAllByUserId(@NotNull final String userId);

    @Override
    @Nullable
    List<Project> findAllSort(
            @NotNull final String userId,
            @NotNull final String order
    );

    @Override
    @Nullable
    Project findOneById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    void removeById(
            @NotNull final String userId,
            @NotNull final String id
    );

    @Override
    long totalCount();

    @Override
    long totalCountByUserId(@NotNull final String userId);

    @Override
    void update(@NotNull final Project project);

    @Override
    void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    );

}
