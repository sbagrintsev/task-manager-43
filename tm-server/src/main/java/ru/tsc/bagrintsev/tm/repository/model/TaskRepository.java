package ru.tsc.bagrintsev.tm.repository.model;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.model.Task;

import java.util.List;

public class TaskRepository extends UserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(
            @NotNull final EntityManager entityManager) {
        super(Task.class, entityManager);
    }

    @Override
    public @Nullable List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.user.id = :userId AND m.project.id = :projectId",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, clazz)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void setProjectId(
            @NotNull final String userId,
            @NotNull final String taskId,
            @Nullable final Project project
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.project = :project " +
                        "WHERE m.user.id = :userId " +
                        "AND m.id = :id",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("project", project)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .executeUpdate();
    }

}
