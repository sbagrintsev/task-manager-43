package ru.tsc.bagrintsev.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.bagrintsev.tm.api.sevice.IConnectionService;
import ru.tsc.bagrintsev.tm.api.sevice.dto.IUserOwnedServiceDTO;
import ru.tsc.bagrintsev.tm.dto.model.AbstractWBSModelDTO;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.exception.entity.ModelNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.bagrintsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.bagrintsev.tm.exception.field.DescriptionIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IdIsEmptyException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectStatusException;
import ru.tsc.bagrintsev.tm.exception.field.NameIsEmptyException;

import java.util.Collection;
import java.util.List;

public abstract class AbstractUserOwnedServiceDTO<M extends AbstractWBSModelDTO, R extends IUserOwnedRepositoryDTO<M>> extends AbstractServiceDTO<M, R> implements IUserOwnedServiceDTO<M> {

    public AbstractUserOwnedServiceDTO(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public abstract M add(@Nullable String userId, M record) throws ModelNotFoundException, IdIsEmptyException;

    @Override
    public abstract M changeStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    ) throws IncorrectStatusException, IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException, ModelNotFoundException;

    @Override
    public abstract void clear(@Nullable String userId) throws IdIsEmptyException;

    @Override
    public abstract M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    ) throws DescriptionIsEmptyException, IdIsEmptyException, NameIsEmptyException, ModelNotFoundException;

    @Override
    public abstract M create(
            @Nullable String userId,
            @Nullable String name
    ) throws NameIsEmptyException, IdIsEmptyException, ModelNotFoundException;

    @Override
    public abstract boolean existsById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException;

    @Override
    @NotNull
    public abstract List<M> findAll(@Nullable String userId) throws IdIsEmptyException;

    @Override
    @NotNull
    public abstract List<M> findAll(
            @Nullable String userId,
            @Nullable Sort sort
    ) throws IdIsEmptyException;

    @Override
    public abstract M findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException;

    @NotNull
    protected String getQueryOrder(@NotNull final Sort sort) {
        if (sort.equals(Sort.BY_NAME)) return "name";
        else if (sort.equals(Sort.BY_STATUS)) return "status";
        else if (sort.equals(Sort.BY_STARTED)) return "date_started";
        else return "date_created";
    }

    @Override
    public abstract M removeById(
            @Nullable String userId,
            @Nullable String id
    ) throws IdIsEmptyException, TaskNotFoundException, ProjectNotFoundException;

    @Override
    @NotNull
    public abstract Collection<M> set(@NotNull Collection<M> records);

    @Override
    public abstract long totalCount(@Nullable String userId) throws IdIsEmptyException;

    @Override
    public abstract M updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) throws DescriptionIsEmptyException, NameIsEmptyException, IdIsEmptyException, TaskNotFoundException;

}
