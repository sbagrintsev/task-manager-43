package ru.tsc.bagrintsev.tm.api.sevice;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import org.jetbrains.annotations.NotNull;

public interface IConnectionService {

    @NotNull EntityManager getEntityManager();

    @NotNull EntityManagerFactory getEntityManagerFactory();

}
