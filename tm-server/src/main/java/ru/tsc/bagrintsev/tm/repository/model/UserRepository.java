package ru.tsc.bagrintsev.tm.repository.model;

import jakarta.persistence.EntityManager;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.repository.model.IUserRepository;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.model.User;

import java.util.List;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(
            @NotNull final EntityManager entityManager
    ) {
        super(User.class, entityManager);
    }

    @Override
    public @Nullable User findByEmail(@NotNull final String email) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.email = :email",
                clazz.getSimpleName());
        @NotNull final List<User> result = entityManager
                .createQuery(jpql, clazz)
                .setParameter("email", email)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public @Nullable User findByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format(
                "FROM %s m WHERE m.login = :login",
                clazz.getSimpleName());
        @NotNull final List<User> result = entityManager
                .createQuery(jpql, clazz)
                .setParameter("login", login)
                .getResultList();
        if (result.isEmpty()) return null;
        return result.get(0);
    }

    @Override
    public boolean isEmailExists(@NotNull String email) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.email = :email",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public boolean isLoginExists(@NotNull String login) {
        @NotNull final String jpql = String.format(
                "SELECT count(*) = 1 FROM %s m WHERE m.login = :login",
                clazz.getSimpleName());
        return entityManager
                .createQuery(jpql, Boolean.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format(
                "DELETE FROM %s m WHERE m.login = :login",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void setParameter(@NotNull final User user) {
        entityManager.merge(user);
    }

    @Override
    public void setRole(
            @NotNull final String login,
            @NotNull final Role role
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.role = :role " +
                        "WHERE m.login = :login",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("role", role)
                .setParameter("login", login)
                .executeUpdate();
    }

    @Override
    public void setUserPassword(
            @NotNull final String login,
            @NotNull final String password,
            final byte @NotNull [] salt
    ) {
        @NotNull final String jpql = String.format("" +
                        "UPDATE %s m " +
                        "SET m.passwordHash = :password, " +
                        "m.passwordSalt = :salt " +
                        "WHERE m.login = :login",
                clazz.getSimpleName());
        entityManager
                .createQuery(jpql)
                .setParameter("login", login)
                .setParameter("password", password)
                .setParameter("salt", salt)
                .executeUpdate();
    }

}
